# Delete-Account for Connect



**[FUNZIONALITA']**<br />

Questo plugin permette, lato admin, di inserire i testi che verrano mostrati agli utenti quando quest'ultimo decideranno di cancellarsi dal forum:<br/>
In particolare,la pagina del plugin è disiva in due parti. La prima parte:


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-admin-inserisci-testi.png)<br/>

A sua volta si divide in due parti. La prima (in nero):<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-admin-inserisci-testi-1.png)<br/>

Permette di inserire il testo che verrà visualizzato da un utente quando quest'ultimo deciderà di cancellarsi e cliccherà sul bottone "Delete Account" esempio:<br/>


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-delete.png)<br/>

Una volta che si inserisce la motivazione l'utente verrà rispedito su una pagina:<br/>
![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-utente-cancellato.png)<br/>

il cui testo che verrà visualizzato potrà essere inserito nella seconda parte della prima parte della pagina del plugin:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-admin-inserisci-testi-2.png)<br/>

Nella seconda parte della pagina del plugin verrà visualizzati tutti gli utenti che hanno deciso di cancellarsi dal forum e le relative motivazioni:<br/>

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-delete-account/raw/demaoui/screenshot/connect-delete-account-lista-utenti-cancellati.png)<br/>



**[TEST]**<br />

Per provare il plugin basta creare un nuovo account, pubblicare un post nel forum e poi cancellare l'account. In particolare bisogna verificare:


* Che l'username con cui era stato scritto il post cambi in "user_del_"
* Che l'email con cui si era registrato l'utente cancellatosi è di nuovo usabile
* Che nessun utente si può registrare con l'username vecchio dell'utente cancellato.



 




















