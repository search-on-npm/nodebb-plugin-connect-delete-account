'use strict';

var db = require.main.require('./src/database'),
	async = require.main.require('async'),
	user = require.main.require('./src/user'),
	websockets = require.main.require('./src/socket.io'),
	utils = require.main.require('./public/src/utils'),
	winston = require.main.require('winston');

var Sockets = {};

var hash = "connect-delete-account";
var hash_conteggio_eliminati = "connect-delete-account-conteggio-eliminati";
var username_modifica = 'user_del_';
var slug_modifica = "user-del-";
var start = 0;
var stop = -1;

//Permette di salvare i vari testi che verranno visualizzati quando
//l'utente si cncella
Sockets.saveTesto = function(socket, data, callback) {
	var testo_iniziale = data.testo_iniziale;
	var testo_finale = data.testo_finale;
	var testo_titolo_post_cancellazione = data.titolo_post_cancellazione;
	var testo_corpo_cancellazione = data.corpo_post_cancellazione;
	winston.verbose('[nodebb-plugin-connect-delete-account] Testo iniziale: ' + testo_iniziale + ' Testo Finale: ' + testo_finale + ' Testo titolo post cancellazione: ' + testo_titolo_post_cancellazione + " Testo corpo post cancellazione: " + testo_corpo_cancellazione);
	db.setObject(hash, {
		'testo_iniziale': testo_iniziale,
		'testo_finale': testo_finale,
		'testo_titolo_post_cancellazione': testo_titolo_post_cancellazione,
		'testo_corpo_cancellazione': testo_corpo_cancellazione
	}, function(err) {
		if (err) {
			return err;
		}
		return callback(null, true);
	});

};
//Ritorna i testi precedentemente inseriti
Sockets.getTestiDeleteAccount = function(socket, data, callback) {
	db.getObject(hash, callback);

};
//Cancella l'utente con username "XXX"
// 1)Salvare la motivazione del perchè l'utente di cancella
// 2)Salvare l'username dell'utente che si sta per cancellare
// 3)Prendere un "id" che rappresenta il numero degli utenti cancellati
// 4)Modificare l'username dell'utente in "user_del_" + id precedentemente ottenuto
// 5) Aggiornare il profilo dell'utente corrente eliminando l'email
// 6) Creare un nuovo utente con username corrente il vecchio username
Sockets.modifica_username = function(socket, data, callback) {
	var username = data.username;
	var spiegazione_uscita = data.textarea;
	var numero_eliminati_utenti = 0;
	var id_corrente = 0;
	async.waterfall([
		//Salvataggio motivazione
		function(next) {
			winston.verbose("[nodebb-plugin-connect-delete-account] MOTIVAZIONE USCITA " + JSON.stringify(spiegazione_uscita));
			db.setObject(hash + ":motivazione:" + username, {
				'motivazione': spiegazione_uscita
			}, function(err) {
				if (err) {
					return next(err);

				}
				next(null);
			});
		},
		//Salvataggio username cancellato
		function(next) {
			db.sortedSetAdd(hash + ":cancellati", Date.now(), username, next);
		},
		function(next) {
			user.getUidByUsername(username, next);
		},
		//Ottengo il numero degli utenti cancellati che tengo come id
		function(id, next) {
			id_corrente = id;
			db.getObject('connect-delete-account-conteggio-eliminati', function(err, numero_eliminati) {

				if (!numero_eliminati) {
					next(null, 0);
					//db.setObject('connect-delete-account-conteggio-eliminati',1,function());

				} else {
					next(null, numero_eliminati.numero_eliminati);
				}
			})

		},
		//Aggiorna numero utente già cancellato
		function(numero_eliminati, next) {
			var aggiorna_count_elimina = parseInt(numero_eliminati) + 1;
			db.setObject('connect-delete-account-conteggio-eliminati', {
				'numero_eliminati': aggiorna_count_elimina
			}, function(err) {
				if (err) {
					return next(err);
				}
				numero_eliminati_utenti = numero_eliminati;
				next(null, numero_eliminati);
			});

		},

		function(numero_eliminati, next) {
			user.getUserData(id_corrente, function(err, utente) {
				if (err) {
					return next(err);
				}
				next(null, numero_eliminati);
			})
		},
		function(numero_eliminati, next) {
			var username_modificati = username_modifica + numero_eliminati;
			var slug_modificati = slug_modifica + numero_eliminati;
			user.updateProfile(id_corrente, {
				'username': username_modificati,
				'slug': slug_modificati,
				'email': '',
				'password': username_modificati
			}, function(err, update_result) {
				if (err) {
					return next(err);

				}
				next();
			});
		},
		//creo un nuovo utente così NodeBB farà i controlli lui quando un utente
		//si registra e utilizza  come username lo stesso username dell'utente
		//corrente
		function(next) {
			user.create({
				'username': username,
				'userslug': utils.slugify(username),
				'email': '',
				'password': '12345678'
			}, next);
		},

		function(user_create, next) {
			//console.log("FUORI SETTE "+JSON.stringify(update_results));
			user.getUserData(id_corrente, function(err, utente) {
				if (err) {
					return next(err);
				}
				next(null);
			});
		},

	], function(error, id_corrente) {
		if (error) {
			return callback(error);
		}
		return callback(null);

	});

};
//Una volta letta la motivazione dall'admin è possibile cancellare
//in maniera definitiva la coppia utente-motivazione 
Sockets.deleteUtenteCancellato = function(socket, data, callback) {
	var lista_username_selezionati = data.lista_username_selezionati;
	var uid_socket = socket.uid;
	async.each(lista_username_selezionati, function(username, prossima) {
		async.waterfall([
			function(next) {
				db.sortedSetRemove(hash + ":cancellati", username, next);
			},
			function(next) {
				db.delete(hash + ":motivazione:" + username, next);
			}
		], function(err) {
			if (err) {
				return callback(err);
			}
			prossima();
		});

	}, function(err) {
		if (err) {
			return callback(err);
		}
		websockets.in('uid_' + uid_socket).emit('connectdeleteaccount:rimuoviselezionati', {
			'lista_username_selezionati': lista_username_selezionati
		});
		return callback(null, true);
	});

};
//Controlla se l'utente con quell'username è già stato cancellato
Sockets.usernameGiaCancellato = function(socket, data, callback) {
	if (!data || !data.username) {
		return callback(null, false);
	}
	var username = data.username.trim().toLowerCase();
	async.waterfall([
		function(next) {
			db.getSortedSetRange(hash + ":cancellati", start, stop, next);
		},
		function(username_cancellati, next) {
			if (!username_cancellati) {
				return callback(null, false);
			}
			var bool = 0;
			for (var i = 0; i < username_cancellati.length; i++) {
				if (username_cancellati[i].trim().toLowerCase() == username) {
					return next(null, true);
				}

			}
			if (bool == 0) {
				return next(null, false);
			}
		}
	], function(err, username_cancellato) {
		if (err) {
			return callback(err);
		}
		return callback(null, username_cancellato);
	});

};


module.exports = Sockets;