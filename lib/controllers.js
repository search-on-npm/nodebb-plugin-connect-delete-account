'use strict';

var db = require.main.require('./src/database'),
	async = require.main.require('async'),
	user = require.main.require('./src/user');

var Controllers = {};

var start = 0;
var stop = -1;
var hash = "connect-delete-account";

Controllers.renderAdminPage = function(req, res, callback) {
	var utente_cancellato_motivazione = [];
	async.waterfall([
			function(next) {
				db.getObject(hash, next);
			},
			function(testi, next) {
				db.getSortedSetRange(hash + ":cancellati", start, stop, function(err, lista_utenti_cancellati) {
					if (err) {
						return next(err);
					}
					next(null, testi, lista_utenti_cancellati);
				});
			},
			function(testi, lista_utenti_cancellati, next) {

				async.each(lista_utenti_cancellati, function(username, chiama) {
					//console.log("USERNAME EACH " + JSON.stringify(username));
					db.getObject(hash + ":motivazione:" + username, function(err, motivazione) {
						if (err) {
							return chiama(err);
						}
						if (motivazione) {
							var motivazione_utente_html = motivazione.motivazione.replace(/\n/g, '<br/>');
							utente_cancellato_motivazione.push({
								'username': username,
								'motivazione': motivazione_utente_html
							});
						}
						chiama();
					});
				}, function(err) {
					if (err) {
						return next(err);
					}
					return next(null, testi, utente_cancellato_motivazione);
				});
			}

		],
		function(err, testi, lista_utenti_cancellati) {
			if (err) {
				return callback(err);
			}
			res.render('admin/plugins/connect-delete-account', {
				'item': testi,
				'lista_utenti': lista_utenti_cancellati.reverse()
			});
		});


};

Controllers.caricaPaginaUtenteCancellato = function(req, res, callback) {


	async.waterfall([
		function(next) {
			if (req && req.user && req.user.uid) {
				var uid = req.user.uid;
				user.getUidByUsername(uid, function(err, id) {
					if (err) {
						return next(err);
					}
					return next(null, id);
				});
			} else {

				return next(null, null);
			}
		},
		function(username, next) {
			db.getObject(hash, function(err, testi) {
				if (err) {
					return next(err);
				}
				if (!testi) {
					return next(err);
				}
				var titolo = testi.testo_titolo_post_cancellazione;
				var corpo = testi.testo_corpo_cancellazione;
				next(null, username, titolo, corpo);

			});
		}
	], function(err, username, titolo, corpo) {
		if (err) {
			return callback(err);
		}
		var titoloHTML = "";
		if (titolo) {
			titoloHTML = titolo.replace(/\n/g, '<br/>');
		}
		var corpoHTML = "";
		if (corpo) {
			corpoHTML = corpo.replace(/\n/g, '<br/>');
		}
		res.render('client/pagina-utente-cancellato', {
			'username': username,
			'titolo': titoloHTML,
			'corpo': corpoHTML
		});

	});


};


module.exports = Controllers;