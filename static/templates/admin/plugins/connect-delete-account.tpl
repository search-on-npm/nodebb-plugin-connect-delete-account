<div class="row">
 <ul class="nav nav-pills" id="menu_admin">
        <li class="active"><a href="#testi" data-toggle="tab">Inserisci Testi</a></li>
        <li><a href="#utentiCancellati" data-toggle="tab">Controlla utenti cancellati </a></li>
    </ul>
    <br/>
    <div class="tab-content">
      <div class="tab-pane fade active in row" id="testi">
    
			<div class="panel-heading">Testo delete Account (Connect)</div>
			<div class="panel-body">
					<blockquote>
						<p>
							Il plugin permette di modificare il testo che verrà mostrato all'utente quando quest'ultimo tenterà di cancellarsi.In particolare, quando l'utente cliccherà sul bottone "delete account" verrà mostrata una finesta in cui ci sarà un testo iniziale, una finestra di input(in cui è possibile inserire la motivazione per cui l'utente ha deciso di cancellarsi) e un testo finale
						</p>
					</blockquote>
					
					<div class="form-group">
					<br>
					<br>
					
					<form  class="delete-account">
						<div class="form-group">
   							<label for="label_testo_iniziale">Testo d'introduzione al form</label>
   							<!-- IF !item -->
   							<textarea class="form-control" id="testo_iniziale"></textarea>
   							<!-- ELSE -->
   								<!-- IF item.testo_iniziale -->
   									<textarea class="form-control" id="testo_iniziale">{item.testo_iniziale}</textarea>
    							<!-- ELSE -->
    								<textarea class="form-control" id="testo_iniziale"></textarea>
    							<!-- ENDIF item.testo_iniziale -->
    						<!-- ENDIF !item -->

  						</div>
  						<br/>
  						<div class="form-group">
   							<label for="label_secondo_testo">Testo finale del form</label>
   							<!-- IF !item -->
    							<textarea class="form-control" id="testo_finale"></textarea>
    						<!-- ELSE -->
    							<!-- IF item.testo_finale -->
    								<textarea class="form-control" id="testo_finale">{item.testo_finale}</textarea>
    							<!-- ELSE -->
    								<textarea class="form-control" id="testo_finale"></textarea>
    							<!-- ENDIF item.testo_finale -->
    						<!-- ENDIF !item -->

  						</div>
              <br/>
              <br/>
              <blockquote>
                Di seguito s'inserisce il testo che dovrà essere visualizzato nella pagina che comparirà una volta cancellato l'account:
              </blockquote>
              <br/>
              <form  class="delete-account">
                <div class="form-group">
                  <label for="label_titolo_testo">Titolo testo pagina post cancellazione</label>
                  <!-- IF !item -->
                    <textarea class="form-control" id="testo_titolo_pagina_post_cancellazione"></textarea>
                    <!-- ELSE -->
                      <!-- IF item.testo_titolo_post_cancellazione -->
                        <textarea class="form-control" id="testo_titolo_pagina_post_cancellazione">{item.testo_titolo_post_cancellazione}</textarea>
                      <!-- ELSE -->
                        <textarea class="form-control" id="testo_finale"></textarea>
                      <!-- ENDIF item.testo_titolo_post_cancellazione -->
                  <!-- ENDIF !item -->
                  <label for="label_corpo_testo">Titolo corpo pagina post cancellazione</label>
                      <!-- IF !item -->
                      <textarea class="form-control" id="testo_corpo_pagina_post_cancellazione"></textarea>
                      <!-- ELSE -->
                        <!-- IF item.testo_corpo_cancellazione -->
                          <textarea class="form-control" id="testo_corpo_pagina_post_cancellazione">{item.testo_corpo_cancellazione}</textarea>
                        <!-- ELSE -->
                          <textarea class="form-control" id="testo_corpo_pagina_post_cancellazione">
                          </textarea>
                        <!-- ENDIF item.testo_corpo_cancellazione -->
                      <!-- ENDIF !item -->


                </div>
              </form>

					</form>
					</div>
          <br/>
          <br/>
				<button id="save" data-action="create" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"> <i class="material-icons" >save</i>
      </button>
			</div>
		</div>
    
    <div class="tab-pane fade in row" id="utentiCancellati">
        <div class="panel-body" id="panel_body">
          <blockquote>
            <p>
              Di seguito verranno elencati tutti gli utenti che hanno deciso di cancellarsi:
            </p>
          </blockquote>
          <br/>
          <br/>
          <!-- IF !lista_utenti.length -->
            <p id="nessun_utente" >Non ci sono utenti cancellati al momento</p>
          <!-- ELSE -->
            <p id="nessun_utente" style="display:none;">Non ci sono utenti cancellati al momento</p>
              <table class="table" id="tabella_username">
              <thead>
                <tr>
                  <th class="text-center">Username Cancellati</th>
                  <th class="text-center"> Motivazione </th>
                  <th class="text-center">Eliminazione/ <button id="seleziona_tutto" type="button" class="btn btn-info btn-xs" name="seleziona">SELEZIONA TUTTI</button><th>
                </tr>
              </thead>
              <tbody id="tbody_tabella_username">
              <!-- BEGIN lista_utenti -->
                <tr id="riga_{lista_utenti.username}">
                  <td align="center"><p>{lista_utenti.username}</p></td>
                  <td align="center"><p>{lista_utenti.motivazione}</p></td>
                  <td style="vertical-align: middle;"> <input id="cancella_{lista_utenti.username}" style="margin:auto;
                  display:block;" type="checkbox" value="" /></td>
                </tr>
              <!-- END lista_utenti -->
              </tbody>
          <!-- ENDIF !lista_utenti.length -->

              </table>
              <button id="cancella" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"> <i class="material-icons" >delete</i>
    </button>
          </div>
    </div>
    </div>
    
	
</div>