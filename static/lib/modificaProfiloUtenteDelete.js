(function() {
	//Serve per far andare a capo la scritta del banner lato user
	$(window).on('action:ajaxify.end', function(event, data) {
		if (new RegExp(/user\/user_del_[0-9]*/).test(data.url)) {
			eliminaIndiciProfilo();
			//$('div[class="text-center profile-meta"]').remove();
			//$('div[class="col-xs-12"]').remove();
		} else {
			checkUserCancellati(data.url.replace(/user\//, ''));
		}

	});

	function checkUserCancellati(username) {
		socket.emit('plugins.connectDeleteAccount.usernameGiaCancellato', {
			'username': username
		}, function(err, cancellato) {
			if (err) {
				app.alertError(err);
			}
			if (Boolean(cancellato) == true) {
				eliminaIndiciProfilo();
			}

		});
	}

	function eliminaIndiciProfilo() {
		$('button[type=button]').remove();
		$('div[class="account-stats"]').remove();
		$('button[class="btn-morph fab plus"]').remove();
		$('i[component="user/status"]').remove();
		$('div[class="row"]').find('div[class="col-xs-12"]').remove();
	}


}());