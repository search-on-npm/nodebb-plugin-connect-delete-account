"use strict";

(function() {
	//Serve per far andare a capo la scritta del banner lato user
	$(window).on('action:ajaxify.end', function(event, data) {
		if (new RegExp(/edit$/).test(data.url)) {
			// Esempio di url http://192.168.56.101:4567/user/admin/edit
			var url = data.url.replace(/\/edit$/, '');
			var username = (url.replace(/user\//i, '')).toLowerCase();
			$(document).ready(function() {
				$('#deleteAccountBtn').off('click');
				$('#deleteAccountBtn').click(function() {

					loadModal();
				});
			});
		}

		function loadModal() {
			ajaxify.loadTemplate('client/modal/connect-delete-account-modal', function(tpl) {
				var modal = bootbox.dialog({
					message: tpl,
					title: 'Operazione di cancellazione di un account',
					buttons: {
						success: {
							label: "Cancella Account",
							className: "btn-primary save",
							callback: function() {
								var textarea = $('#motivazione_cancellazione').val();
								if (textarea) {

									modifica_username_utente(username, textarea);

									//window.location.href = config.relative_path + '/client/pagina-utente-cancellato';

									$.ajax(config.relative_path + '/logout', {
										type: 'POST',
										headers: {
											'x-csrf-token': config.csrf_token
										},
										success: function() {
											window.location.href = config.relative_path + '/client/pagina-utente-cancellato';
										}
									});
								} else {
									app.alertError("Inserire la motivazione per cui si sta cancellando il proprio account");
									return false;
								}

							}
						}
					}
				});
				modal.on('shown.bs.modal', function() {
					socket.emit('plugins.connectDeleteAccount.getTestiDeleteAccount', function(err, testi) {
						if (err) {
							app.alertError(err);
						}
						if (testi) {
							var testo_iniziale = testi.testo_iniziale;
							testo_iniziale = testo_iniziale.replace(/\n/g, '<br/>');
							var testo_finale = testi.testo_finale;
							testo_finale = testo_finale.replace(/\n/g, '<br/>');
							$('#testo_iniziale').html(testo_iniziale);
							$('#testo_finale').html(testo_finale);
						}

					});
				});

			});
		}

		function modifica_username_utente(username, textarea) {

			socket.emit('plugins.connectDeleteAccount.modifica_username', {
				'username': username,
				'textarea': textarea
			}, function(err) {
				if (err) {
					app.alertError(err);
				}

			});
		}

	});


}());