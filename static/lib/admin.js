define('admin/plugins/connect-delete-account', ['settings'], function(Settings) {
	'use strict';
	/* globals $, app, socket, require */

	var ACP = {};
	var colore_errore = 'orange';
	ACP.init = function() {

		$('#save').on('click', function() {
			$('textarea[id^=testo]').removeAttr('style');
			var text_area_iniziale = $('#testo_iniziale');
			var text_area_finale = $('#testo_finale');
			var text_titolo_pagina_post_cancellazione = $('#testo_titolo_pagina_post_cancellazione');
			var text_corpo_pagina_post_cancellazione = $('#testo_corpo_pagina_post_cancellazione');
			var testo_iniziale = text_area_iniziale.val().trim();
			var testo_finale = text_area_finale.val().trim();
			var testo_titolo_pagina_post_cancellazione = text_titolo_pagina_post_cancellazione.val().trim();
			var testo_corpo_pagina_post_cancellazione = text_corpo_pagina_post_cancellazione.val().trim();
			if (testo_iniziale && testo_finale && testo_corpo_pagina_post_cancellazione && testo_titolo_pagina_post_cancellazione) {
				$('textarea[id^=testo]').removeAttr('style');

				socket.emit('plugins.connectDeleteAccount.saveTesto', {
					'testo_iniziale': testo_iniziale,
					'testo_finale': testo_finale,
					'titolo_post_cancellazione': testo_titolo_pagina_post_cancellazione,
					'corpo_post_cancellazione': testo_corpo_pagina_post_cancellazione
				}, function(err, check_salvataggio) {
					if (err) {
						app.alertError(err);
					}
					if (check_salvataggio) {
						app.alertSuccess("Salvataggio avvenuto con successo");
					}


				});
			} else {
				if (!testo_iniziale) {
					app.alertError(" Campo Testo d'introduzione al form VUOTO");
					text_area_iniziale.css('background-color', colore_errore);

				}
				if (!testo_finale) {
					app.alertError("Campo Testo finale del form VUOTO");
					text_area_finale.css('background-color', colore_errore);
				}
				if (!testo_corpo_pagina_post_cancellazione) {
					app.alertError("Campo 'Titolo testo pagina post cancellazione' VUOTO");
					text_corpo_pagina_post_cancellazione.css('background-color', colore_errore);
				}
				if (!testo_titolo_pagina_post_cancellazione) {
					app.alertError("Campo 'Corpo testo pagina post cancellazione' VUOTO");
					text_titolo_pagina_post_cancellazione.css('background-color', colore_errore);
				}
			}


		});
		$('[id=seleziona_tutto]').on('click', function() {
			if ($(this).attr('name') == 'seleziona') {
				$(this).attr('name', 'deseleziona');
				$(this).text("DESELEZIONA TUTTI");
				$('input[type=checkbox]').prop('checked', true);
			} else {
				$(this).attr('name', 'seleziona');
				$(this).text("SELEZIONA TUTTI");
				$('input[type=checkbox]').prop('checked', false);
			}

		});
		$('[id=cancella]').on('click', function() {
			var username_selezionati = $('input[type="checkbox"]:checked');
			if (username_selezionati && username_selezionati.length != 0) {
				var vettore_checked = [];
				username_selezionati.each(function() {
					var username = $(this).attr('id').replace(/cancella_/g, '');
					vettore_checked.push(username);

				});
				socket.emit('plugins.connectDeleteAccount.deleteUtenteCancellato', {
					'lista_username_selezionati': vettore_checked
				}, function(err, flag_rimozione_avvenuta) {
					if (err) {
						app.alertError(err);
					}
				});
			} else {
				app.alertError("Selezionare uno o più username da cancellare");
			}
		});
		socket.on('connectdeleteaccount:rimuoviselezionati', function(params) {
			var vettore_checked = params.lista_username_selezionati;
			for (var i = 0; i < vettore_checked.length; i++) {
				$('tr[id="riga_' + vettore_checked[i] + '"]').remove();
			}
			var numero_tr = $('#tbody_tabella_username').find('tr').length;
			if (numero_tr == 0) {
				$('#tabella_username').remove();
				$('#nessun_utente').show();
			}
		});


	};

	return ACP;
});