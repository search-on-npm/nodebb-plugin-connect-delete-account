"use strict";

var controllers = require('./lib/controllers');
var converter = {};
var db = require.main.require('./src/database'),
	winston = require.main.require('winston'),
	sockets = require('./lib/sockets'),
	SocketPlugins = require.main.require('./src/socket.io/plugins');


SocketPlugins.connectDeleteAccount = sockets;

converter.init = function(params, callback) {

	var router = params.router,
		hostMiddleware = params.middleware,
		hostControllers = params.controllers;

	// We create two routes for every view. One API call, and the actual route itself.
	// Just add the buildHeader middleware to your route and NodeBB will take care of everything for you.

	router.get('/admin/plugins/connect-delete-account', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
	router.get('/api/admin/plugins/connect-delete-account', controllers.renderAdminPage);
	router.get('/client/pagina-utente-cancellato', hostMiddleware.buildHeader, controllers.caricaPaginaUtenteCancellato);
	callback();
};

converter.addAdminNavigation = function(header, callback) {

	header.plugins.push({
		route: '/plugins/connect-delete-account',
		icon: 'fa-file-image-o',
		name: 'Delete-account(Connect)'
	});
	callback(null, header);
};



module.exports = converter;